
import SkinAnalysis from './components/SkinAnalysis';

function App() {
  return (
    <div className='wrapper'>
    <SkinAnalysis />
  </div>
  );
}

export default App;
