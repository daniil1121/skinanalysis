import { NavLink } from "react-router-dom"
import s from "./Form.module.css"

const Form4 = () => {
  return (
    <div className={s.wrapper}>
      <div className={s.text2}>Page 4</div>
      <div>
        <NavLink to={"/skinquestion/3"}>
          <button>back</button>
        </NavLink>
        <NavLink to={"/skinquestion/5"}>
          <button>go</button>
        </NavLink>
      </div>
    </div>
  )
}
export default Form4
