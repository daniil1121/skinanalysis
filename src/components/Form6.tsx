import { NavLink } from "react-router-dom"
import s from "./Form.module.css"

const Form6 = () => {
  return (
    <div className={s.wrapper}>
      <div className={s.text2}>Page 6</div>
      <div>
        <NavLink to={"/skinquestion/5"}>
          <button>back</button>
        </NavLink>
        <NavLink to={"/skinquestion/7"}>
          <button>go</button>
        </NavLink>
      </div>
    </div>
  )
}
export default Form6
