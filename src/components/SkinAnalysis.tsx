import s from "./SkinAnalysis.module.css"
import Form from "./Form"
import Form1 from "./Form1"
import Form2 from "./Form2"
import Form3 from "./Form3"
import Form4 from "./Form4"
import Form5 from "./Form5"
import Form6 from "./Form6"
import Form7 from "./Form7"
import Form8 from "./Form8"
import { BrowserRouter, Route } from "react-router-dom"
import Form9 from "./Form9"
import Form10 from "./Form10"
import Form11 from "./Form11"
import Form12 from "./Form12"
import FormEnd from "./FormEnd"

const SkinAnalysis = () => {
  return (
    <BrowserRouter>
      <div className={s.wrapper}>
        <div className={s.header}>ONLINE SKIN ANALYSIS</div>
        <div className={s.line}></div>
        <Route exact path="/skinquestion" render={() => <Form />} />
        <Route path="/skinquestion/1" render={() => <Form1 />} />
        <Route path="/skinquestion/2" render={() => <Form2 />} />
        <Route path="/skinquestion/3" render={() => <Form3 />} />
        <Route path="/skinquestion/4" render={() => <Form4 />} />
        <Route path="/skinquestion/5" render={() => <Form5 />} />
        <Route path="/skinquestion/6" render={() => <Form6 />} />
        <Route path="/skinquestion/7" render={() => <Form7 />} />
        <Route path="/skinquestion/8" render={() => <Form8 />} />
        <Route path="/skinquestion/9" render={() => <Form9 />} />
        <Route path="/skinquestion/10" render={() => <Form10 />} />
        <Route path="/skinquestion/11" render={() => <Form11 />} />
        <Route path="/skinquestion/12" render={() => <Form12 />} />
        <Route path="/skinquestion/end" render={() => <FormEnd />} />
      </div>
    </BrowserRouter>
  )
}

export default SkinAnalysis
