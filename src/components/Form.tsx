import s from "./Form.module.css"
import { NavLink } from "react-router-dom"
import { useState, useEffect } from "react"

const Form = () => {
  const [initLocalStorage] = useState("")

  useEffect(() => {
    localStorage.setItem("firstName", JSON.stringify(initLocalStorage))
    localStorage.setItem("lastName", JSON.stringify(initLocalStorage))
    localStorage.setItem("email", JSON.stringify(initLocalStorage))
    localStorage.setItem("age", JSON.stringify(initLocalStorage))
    localStorage.setItem("gender", JSON.stringify(initLocalStorage))
    localStorage.setItem("Morning Skin", JSON.stringify(initLocalStorage))
    localStorage.setItem("Breakout frequency", JSON.stringify(initLocalStorage))
    localStorage.setItem("Whiteheads", JSON.stringify(initLocalStorage))
    localStorage.setItem("Large, under-the-surface bumps", JSON.stringify(initLocalStorage))
    localStorage.setItem("Small bumps", JSON.stringify(initLocalStorage))
    localStorage.setItem("Aging signs", JSON.stringify(initLocalStorage))
    localStorage.setItem("Dermatologist", JSON.stringify(initLocalStorage))
    localStorage.setItem("Are you currently using any oral prescriptions for the face", JSON.stringify(initLocalStorage))
    localStorage.setItem("Are you currently using any topical prescriptions for the face", JSON.stringify(initLocalStorage))
    localStorage.setItem("Are you allergic to any skincare ingredients", JSON.stringify(initLocalStorage))
    localStorage.setItem("Time under care of dermatologist", JSON.stringify(initLocalStorage))
    localStorage.setItem("Currently using topical prescriptions", JSON.stringify(initLocalStorage))
    localStorage.setItem("Currently using oral prescriptions", JSON.stringify(initLocalStorage))
    localStorage.setItem("Experiencing clogged pores", JSON.stringify(initLocalStorage))
    localStorage.setItem("Diagnosed skin conditions", JSON.stringify(initLocalStorage))
    localStorage.setItem("Sensitivity to ingredients", JSON.stringify(initLocalStorage))
    localStorage.setItem("Skincare products currently being used", JSON.stringify(initLocalStorage))
    localStorage.setItem("Combination oily or combination dry", JSON.stringify(initLocalStorage))
    localStorage.setItem("Blemish", JSON.stringify(initLocalStorage))
    localStorage.setItem("Breakouts", JSON.stringify(initLocalStorage))
  }, [initLocalStorage])

  return (
    <div className={s.wrapper}>
      <h3 className={s.step}>Step 1 </h3>
      <h2 className={s.text}>Identify Your Skin Type</h2>

      <NavLink to={"/skinquestion/1"}>
        <div className={s.button}>BEGIN</div>
      </NavLink>
    </div>
  )
}
export default Form
