import { NavLink } from "react-router-dom"
import s from "./Form.module.css"

const Form5 = () => {
  return (
    <div className={s.wrapper}>
      <div className={s.text2}>Page 5</div>
      <div>
        <NavLink to={"/skinquestion/4"}>
          <button>back</button>
        </NavLink>
        <NavLink to={"/skinquestion/6"}>
          <button>go</button>
        </NavLink>
      </div>
    </div>
  )
}
export default Form5
