import { NavLink } from "react-router-dom"
import s from "./Form.module.css"

const Form9 = () => {
  return (
    <div className={s.wrapper}>
      <div className={s.text2}>Page 9</div>
      <div>
        <NavLink to={"/skinquestion/8"}>
          <button>back</button>
        </NavLink>
        <NavLink to={"/skinquestion/10"}>
          <button>go</button>
        </NavLink>
      </div>
    </div>
  )
}
export default Form9
