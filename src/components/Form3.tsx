import { NavLink } from "react-router-dom"
import { useState, useEffect, useCallback } from "react"
import s from "./Form.module.css"

const Form3 = () => {
  let a = {
    pageText: {
      text1: "DAILY",
      text2: "WEEKLY",
      text3: "MONTHLY",
      text4: "RARELY",
      text5: "NO BREAKOUTS",
    },
  }
  //LC - Local Storage
  const valueBreakoutInLS = localStorage.getItem("Breakout frequency")
  let valueBreakoutJSON = valueBreakoutInLS ? valueBreakoutInLS : ""
  const valueBreakout = JSON.parse(valueBreakoutJSON)

  const [valueBreakoutForLS, setPage4] = useState(valueBreakout)

  useEffect(() => {
    localStorage.setItem("Breakout frequency", JSON.stringify(valueBreakoutForLS))
  }, [valueBreakoutForLS])

  useEffect(() => {
    let inputArr = document.querySelectorAll("input")
    if (inputArr.length > 0) {
      for (let i = 0; i < inputArr.length; i++) {
        if (inputArr[i].value == valueBreakoutForLS) {
          inputArr[i].setAttribute("checked", "true")
        } else {
          inputArr[i].removeAttribute("checked")
        }
      }
    }
  })
  let addBorderBall1 = () => {
    let ball = document.getElementById("ball1")
    ball?.setAttribute("class", `${s.ball1Hover}`)
  }
  let removeBorderBall1 = () => {
    if (a.pageText.text1 !== valueBreakoutForLS) {
      let ball = document.getElementById("ball1")
      ball?.setAttribute("class", `${s.ball1}`)
    }
  }

  let addBorderBall2 = () => {
    let ball = document.getElementById("ball2")
    ball?.setAttribute("class", `${s.ball2Hover}`)
  }
  let removeBorderBall2 = () => {
    if (a.pageText.text2 !== valueBreakoutForLS) {
      let ball = document.getElementById("ball2")
      ball?.setAttribute("class", `${s.ball2}`)
    }
  }

  let addBorderBall3 = () => {
    let ball = document.getElementById("ball3")
    ball?.setAttribute("class", `${s.ball3Hover}`)
  }
  let removeBorderBall3 = () => {
    if (a.pageText.text3 !== valueBreakoutForLS) {
      let ball = document.getElementById("ball3")
      ball?.setAttribute("class", `${s.ball3}`)
    }
  }
  let addBorderBall4 = () => {
    let ball = document.getElementById("ball4")
    ball?.setAttribute("class", `${s.ball4Hover}`)
  }
  let removeBorderBall4 = () => {
    if (a.pageText.text4 !== valueBreakoutForLS) {
      let ball = document.getElementById("ball4")
      ball?.setAttribute("class", `${s.ball4}`)
    }
  }

  useEffect(() => {
    const valueBreakoutInLS = localStorage.getItem("Breakout frequency")
    let valueBreakoutJSON = valueBreakoutInLS ? valueBreakoutInLS : ""
    const valueBreakout = JSON.parse(valueBreakoutJSON)

    if (valueBreakout === a.pageText.text1) {
      let ball = document.getElementById("ball1")
      ball?.setAttribute("class", `${s.ball1Hover}`)
    }
    if (valueBreakout === a.pageText.text2) {
      let ball = document.getElementById("ball2")
      ball?.setAttribute("class", `${s.ball2Hover}`)
    }
    if (valueBreakout === a.pageText.text3) {
      let ball = document.getElementById("ball3")
      ball?.setAttribute("class", `${s.ball3Hover}`)
    }
    if (valueBreakout === a.pageText.text4) {
      let ball = document.getElementById("ball4")
      ball?.setAttribute("class", `${s.ball4Hover}`)
    }

    if (valueBreakout !== a.pageText.text1) {
      let ball = document.getElementById("ball1")
      ball?.setAttribute("class", `${s.ball1}`)
    }
    if (valueBreakout !== a.pageText.text2) {
      let ball = document.getElementById("ball2")
      ball?.setAttribute("class", `${s.ball2}`)
    }
    if (valueBreakout !== a.pageText.text3) {
      let ball = document.getElementById("ball3")
      ball?.setAttribute("class", `${s.ball3}`)
    }
    if (valueBreakout !== a.pageText.text4) {
      let ball = document.getElementById("ball4")
      ball?.setAttribute("class", `${s.ball4}`)
    }
  })

  return (
    <div className={s.wrapper}>
      <h3 className={s.step}>Step 3 out of 13</h3>
      <h2 className={s.text}>Identify Your Skin Type</h2>
      <div className={s.text2}>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime iste sed, molestiae porro voluptatibus quia aliquam velit aspernatur veniam,
        tempora alias aperiam deserunt dolores ea magni illo incidunt cumque ullam? Lorem ipsum dolor sit amet consectetur adipisicing elit.
      </div>

      <div className={s.page4LineForBall}>
        <div id="ball1" className={s.ball1}></div>
        <div id="ball2" className={s.ball2}></div>
        <div id="ball3" className={s.ball3}></div>
        <div id="ball4" className={s.ball4}></div>
      </div>

      <div className={s.page4RadioForBall}>
        <label onMouseOver={() => addBorderBall1()} onMouseLeave={() => removeBorderBall1()} htmlFor="1">
          <input onChange={(e) => setPage4(e.target.value)} id="1" name="page3InSkinAnalysis" type="radio" value={a.pageText.text1} />
          {a.pageText.text1}
        </label>
        <label onMouseOver={() => addBorderBall2()} onMouseLeave={() => removeBorderBall2()} htmlFor="2">
          <input onChange={(e) => setPage4(e.target.value)} id="2" name="page3InSkinAnalysis" type="radio" value={a.pageText.text2} />
          {a.pageText.text2}
        </label>
        <label onMouseOver={() => addBorderBall3()} onMouseLeave={() => removeBorderBall3()} htmlFor="3">
          <input onChange={(e) => setPage4(e.target.value)} id="3" name="page3InSkinAnalysis" type="radio" value={a.pageText.text3} />
          {a.pageText.text3}
        </label>
        <label onMouseOver={() => addBorderBall4()} onMouseLeave={() => removeBorderBall4()} htmlFor="4">
          <input onChange={(e) => setPage4(e.target.value)} id="4" name="page3InSkinAnalysis" type="radio" value={a.pageText.text4} />
          {a.pageText.text4}
        </label>
        <label htmlFor="5">
          <input onChange={(e) => setPage4(e.target.value)} id="5" name="page3InSkinAnalysis" type="radio" value={a.pageText.text5} />
          {a.pageText.text5}
        </label>
      </div>
      <div className={s.AllButtons}>
        <div>
          <NavLink to={"/skinquestion/2"}>
            <div className={s.buttonLeft}>PREVIOUS</div>
          </NavLink>
        </div>
        <div>
          {valueBreakoutForLS ? (
            <NavLink to={"/skinquestion/4"}>
              <div className={s.buttonRight}>NEXT</div>
            </NavLink>
          ) : (
            <div
              className={s.buttonRight}
              onClick={() => {
                alert("заполните все поля")
              }}
            >
              NEXT
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
export default Form3
