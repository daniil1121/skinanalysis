import s from "./Form.module.css"
import { NavLink } from "react-router-dom"
import { useState, useEffect } from "react"

const Form1 = () => {
  const valueEmailinLS = localStorage.getItem("email")
  let valueEmailJSON = valueEmailinLS ? valueEmailinLS : ""
  const valueEmail = JSON.parse(valueEmailJSON)

  const valueNameInLS = localStorage.getItem("firstName")
  let valueNameJSON = valueNameInLS ? valueNameInLS : ""
  const valueName = JSON.parse(valueNameJSON)

  const valueLastNameInLS = localStorage.getItem("lastName")
  let valueLastNameJSON = valueLastNameInLS ? valueLastNameInLS : ""
  const valueLastName = JSON.parse(valueLastNameJSON)

  const savedAge = localStorage.getItem("age")
  let selectAge = savedAge ? savedAge : ""
  const initialSelectAge = JSON.parse(selectAge)

  const savedGender = localStorage.getItem("gender")
  let selectGender = savedGender ? savedGender : ""
  const initialSelectGender = JSON.parse(selectGender)

  const [email, setEmail] = useState(valueEmail)
  const [name, setName] = useState(valueName)
  const [family, setFamyly] = useState(valueLastName)
  const [age, setAge] = useState(initialSelectAge)
  const [gender, setGender] = useState(initialSelectGender)

  useEffect(() => {
    localStorage.setItem("age", JSON.stringify(age))
  }, [age])

  useEffect(() => {
    localStorage.setItem("gender", JSON.stringify(gender))
  }, [gender])

  useEffect(() => {
    localStorage.setItem("email", JSON.stringify(email))
  }, [email])

  useEffect(() => {
    localStorage.setItem("firstName", JSON.stringify(name))
  }, [name])

  useEffect(() => {
    localStorage.setItem("lastName", JSON.stringify(family))
  }, [family])

  return (
    <div className={s.wrapper}>
      <h3 className={s.step}>Step 2</h3>
      <h2 className={s.text}>Identify Your Skin Type</h2>
      <div className={s.text2}>
        <p>
          At Mario Badescu Skin Care, we believe that there is no universal, one size fits all skin care regimen that will leave everybody with
          glowing, beautiful skin. There are many factors involved in determining the proper skin care regimen.
        </p>
        <p>
          There are many different types of skin, including Oily, Dry, Sensitive, and the most common, Combination. On top of natural skin type,
          proper skin care must also consider a number of factors such as age, gender, skin tone, skin care history, and lifestyle. All of these
          factors mean that there are literally thousands of possible skin care regimens, each one suited for a different person.
        </p>
        <p>
          This skin care consulting questionnaire takes all of the above information into account to form specialized skin care product
          recommendations that are customized for you.
        </p>
        <p>The skin analysis takes only a moment, but can offer invaluable skin care advice that will leave your skin clear and your face glowing.</p>
        <p>Before we begin, please fill out the fields below. All fields marked with an asterisk (*) are required.</p>{" "}
      </div>
      <div className={s.line}></div>
      <div className={s.allInput}>
        <div>
          <div className={s.label}>Email*:</div>
          <div>
            <input
              className={s.input}
              value={valueEmail === email ? valueEmail : email}
              placeholder="Email"
              onChange={(e) => setEmail(e.target.value)}
              type="text"
            />
          </div>
        </div>
        <div>
          <div className={s.label}>First Name*:</div>
          <div>
            <input
              className={s.input}
              value={valueName === name ? valueName : name}
              placeholder="First Name"
              onChange={(e) => setName(e.target.value)}
              type="text"
            />
          </div>
        </div>
        <div>
          <div className={s.label}>Last Name*:</div>
          <div>
            <input
              className={s.input}
              value={valueLastName === family ? valueLastName : family}
              placeholder="Last Name"
              onChange={(e) => setFamyly(e.target.value)}
              type="text"
            />
          </div>
        </div>
        <div>
          <div className={s.label}>Age*:</div>
          <div>
            <select className={s.select} onChange={(e) => setAge(e.target.value)} value={initialSelectAge === age ? initialSelectAge : age}>
              <option value={""}>--Please Select--</option>
              <option value={"under 18"}>under 18</option>
              <option value={"18-25"}>18-25</option>
              <option value={"26-35"}>26-35</option>
              <option value={"36-45"}>36-45</option>
              <option value={"46-55"}>46-55</option>
              <option value={"56-65"}>56-65</option>
              <option value={"over 65"}>over 65</option>
            </select>
          </div>

          <div>
            <div className={s.label}>Gender*:</div>
            <div>
              <select
                className={s.select}
                onChange={(e) => setGender(e.target.value)}
                value={initialSelectGender === gender ? initialSelectGender : gender}
              >
                <option value={""}>--Please Select--</option>
                <option value={"Male"}>Male</option>
                <option value={"Female"}>Female</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div className={s.AllButtons}>
        <div>
          <NavLink to={"/skinquestion"}>
            <div className={s.buttonLeft}>PREVIOUS</div>
          </NavLink>
        </div>
        <div>
          {gender && family && name && email && age ? (
            <NavLink to={"/skinquestion/2"}>
              <div className={s.buttonRight}>NEXT</div>
            </NavLink>
          ) : (
            <div
              className={s.buttonRight}
              onClick={() => {
                alert("заполните все поля")
              }}
            >
              NEXT
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
export default Form1
