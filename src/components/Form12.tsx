import { NavLink } from "react-router-dom"
import s from "./Form.module.css"

const Form12 = () => {
  return (
    <div className={s.wrapper}>
      <div className={s.text2}>Page 12</div>
      <div>
        <NavLink to={"/skinquestion/12"}>
          <button>back</button>
        </NavLink>
        <NavLink to={"/skinquestion/end"}>
          <button>go</button>
        </NavLink>
      </div>
    </div>
  )
}
export default Form12
