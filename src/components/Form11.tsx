import { NavLink } from "react-router-dom"
import s from "./Form.module.css"

const Form11 = () => {
  let end = true
  return (
    <div className={s.wrapper}>
      <div className={s.text2}>Page 11</div>
      <div>
        <NavLink to={"/skinquestion/11"}>
          <button>back</button>
        </NavLink>

        {end ? (
          <NavLink to={"/skinquestion/12"}>
            <button>go</button>
          </NavLink>
        ) : (
          <NavLink to={"/skinquestion/end"}>
            <button>go</button>
          </NavLink>
        )}
      </div>
    </div>
  )
}
export default Form11
