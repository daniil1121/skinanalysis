import { NavLink } from "react-router-dom"
import s from "./Form.module.css"

const Form10 = () => {
  return (
    <div className={s.wrapper}>
      <div className={s.text2}>Page 10</div>
      <div>
        <NavLink to={"/skinquestion/9"}>
          <button>back</button>
        </NavLink>
        <NavLink to={"/skinquestion/11"}>
          <button>go</button>
        </NavLink>
      </div>
    </div>
  )
}
export default Form10
