import { NavLink } from "react-router-dom"
import s from "./Form.module.css"
import { useState, useEffect } from "react"

const Form2 = () => {
  const savedEmail = localStorage.getItem("Morning Skin")
  let textEmail = savedEmail ? savedEmail : ""
  const initialValuePage3 = JSON.parse(textEmail)

  const [page3, setPage3] = useState(initialValuePage3)

  useEffect(() => {
    localStorage.setItem("Morning Skin", JSON.stringify(page3))
  }, [page3])

  useEffect(() => {
    let a = document.querySelectorAll("input")
    if (a.length > 0) {
      for (let i = 0; i < a.length; i++) {
        if (a[i].value == initialValuePage3) {
          a[i].setAttribute("checked", "true")
        }
      }
    }
  })
  let a = {
    page2: {
      text1: "Shiny and oily",
      text2: "Combination",
      text3: "Dry",
      text6: "Dry",
      text4: "Blotchy",
    },
  }
  return (
    <div className={s.wrapper}>
      <div className={s.padioWrapper}>
        <h3 className={s.step}>Step 3</h3>
        <h2 className={s.text}>How does your skin look and feel in the morning?</h2>
        <div className={s.line}></div>

        <div className={s.Allradios}>
          <label htmlFor="1">
            <input id="1" onChange={(e) => setPage3(e.target.value)} name="page2InSkinAnalysis" type="radio" value={a.page2.text1} />
            {a.page2.text1}
          </label>
          <label htmlFor="2">
            <input id="2" onChange={(e) => setPage3(e.target.value)} name="page2InSkinAnalysis" type="radio" value={a.page2.text2} />
            {a.page2.text2}
          </label>
          <label htmlFor="3">
            <input id="3" onChange={(e) => setPage3(e.target.value)} name="page2InSkinAnalysis" type="radio" value={a.page2.text3} />
            {a.page2.text3}
          </label>
          <label htmlFor="4">
            <input id="4" onChange={(e) => setPage3(e.target.value)} name="page2InSkinAnalysis" type="radio" value={a.page2.text4} />
            {a.page2.text4}
          </label>
        </div>
      </div>

      <div className={s.AllButtons}>
        <div>
          <NavLink to={"/skinquestion/1"}>
            <div className={s.buttonLeft}>PREVIOUS</div>
          </NavLink>
        </div>
        <div>
          {page3 ? (
            <NavLink to={"/skinquestion/3"}>
              <div className={s.buttonRight}>NEXT</div>
            </NavLink>
          ) : (
            <div
              className={s.buttonRight}
              onClick={() => {
                alert("заполните все поля")
              }}
            >
              NEXT
            </div>
          )}
        </div>
      </div>
      <div className={s.text3}>
        <p>
          Knowing your skin's needs is the single most important prerequisite for taking the best care of it. By understanding your skin type you can
          take the necessary steps to correct any problems and address any concerns.
        </p>
        <h4>Not sure what your skin type is?</h4>
        <p>
          Observe your skin first thing in the morning. Are you shiny in the T-zone or all over? If so, you probably have oily skin. Does your skin
          have minimal shine or dry patches? It’s likely you have combination skin. Do you wake up with tight, flaky skin? You have dry skin. Is your
          skin red or irritated in the morning? If so, you have blotchy, sensitive skin.
        </p>
        <p>Learn more about your skin type below:</p>
        <h4>Dry Skin</h4>
        <p>
          Dry skin occurs when the oil glands don't produce enough oil to properly lubricate the skin. Dry skin is thin and often flaky, with
          virtually invisible pores; it’s also prone to visible signs of aging like fine lines and wrinkles.
        </p>
        <h4>Combination Skin</h4>
        <p>
          Combination skin is exactly what the name implies: It’s a combination of dry, oily or normal areas on the skin that may vary day to day,
          seasonally, or with environmental or dietary changes. Most people fall into this category and may need to change products seasonally to
          adapt to the changing needs of the skin.
        </p>
        <h4>Blotchy Skin</h4>
        <p>
          Blotchiness, persistent redness, and dryness are common characteristics of sensitive skin. Because Sensitive skin reacts relatively easily
          in response to external factors, it requires special care. Look for products formulated with mild, soothing formulas.
        </p>
      </div>
    </div>
  )
}

export default Form2
