import { NavLink } from "react-router-dom"
import s from "./Form.module.css"

const FormEnd = () => {
  return (
    <div className={s.wrapper}>
      <div className={s.text2}>Thank you</div>
    </div>
  )
}
export default FormEnd
